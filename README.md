# Taxonomy

## Sources
Source: [https://meshb.nlm.nih.gov/treeView](https://meshb.nlm.nih.gov/treeView) (xml: [https://www.nlm.nih.gov/mesh/filelist.html](https://www.nlm.nih.gov/mesh/filelist.html) or [ftp://nlmpubs.nlm.nih.gov/online/mesh/MESH_FILES/xmlmesh/](ftp://nlmpubs.nlm.nih.gov/online/mesh/MESH_FILES/xmlmesh/))
Source: [https://clinia.ca/fr/services](https://clinia.ca/fr/services)

## Documents
- **Health occupation** (H2) :Professions or other business activities directed to the cure and prevention of disease (ex: Dentistry, Medecine, Nursing)
    - **Health Practices**: Precision on the Health occupation (ex: Pediatrics under Medecine)
- **Persons** (M01): Persons as individuals that segment on characteristics (ex: Men, Minor, Child, Smoker, Jehovah's Witnesses, Homeless Persons)
- **Population Characteristics** (N01): Qualities and characterization of various types of populations within a social or geographic group, with emphasis on demography, health status, and socioeconomic factors.
- **Diseases** (C)
- **Analytical, Diagnostic and Therapeutic Techniques, and Equipment** (E)
- **Psychiatry and Psychology** (F)

